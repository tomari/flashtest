#include <stdexcept>
#include <iostream>
#include <string>
#include <random>
#include <memory>
#include <boost/filesystem.hpp>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
namespace fs=boost::filesystem;
#include <boost/program_options.hpp>
namespace po=boost::program_options;

#include "FileDescr.hpp"
#include "FlashTest.hpp"
#include "FlashWriter.hpp"
#include "FlashVerifier.hpp"

extern int main(int argc, char *argv[]) {
	po::options_description desc("Allowed options");
	desc.add_options()
		("help","show help message")
		("read,r",po::value<std::string>(),"verify file")
		("write,w",po::value<std::string>(),"create test file")
		("size,s",po::value<long>(),"file size to fill, in megabytes")
		("ratio,p",po::value<long>(),"percentage of free space to fill, in %")
	;
	po::variables_map vm;
	po::store(po::command_line_parser(argc,argv).options(desc).run(),vm);
	po::notify(vm);

	if(vm.count("help")){
		std::cout<<desc<<std::endl;
		exit(0);
	}else if(vm.count("read")){
		auto& path=vm["read"].as<std::string>();
		auto fv=FlashVerifier(path.c_str());
		exit(fv.run());
	}else if(vm.count("write")){
		auto& path=vm["write"].as<std::string>();
		std::unique_ptr<Runnable> fw;
		if(vm.count("size")){ // fill size specified in megabytes
			uintmax_t mb=vm["size"].as<long>();
			fw=std::make_unique<FlashWriterMB>(path.c_str(),mb);
		}else{
			auto percent=15./16.;
			if(vm.count("ratio")){
				auto p=vm["ratio"].as<long>();
				percent=static_cast<double>(p)/100.;
			}
			fw=std::make_unique<FlashWriterPercentage>(path.c_str(),percent);
		}
		exit(fw->run());
	}else{
		std::cout<<desc<<std::endl;
		exit(0);
	}
	exit(0);
}
