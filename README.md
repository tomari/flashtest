# flashtest

[![CircleCI](https://circleci.com/gh/tomari/flashtest.svg?style=svg)](https://circleci.com/gh/tomari/flashtest)

FlashTest is a tool to verify data integrity on flash memory based storage system.
It can also be used on regular spinning disks though.
FlashTest writes random sequence of bytes to a file that fills up ~94 % of free space in the filesystem of a specified path.
Here, you can optionally detach the storage and keep it in shelf for weeks.
Later, FlashTest can be instructed to verify the contents of the file, and report errors if found.

Writes are buffered, as it will decrease the damage to the flash it done otherwise.

## Requirements

- CentOS 7 or Debian 8 [jessie] -class 64-bit Linux system
- Boost::filesystem library.
- GCC 4.8 -class C++ compiler (C++11 features are required)
- Preferably, the flash memory should be formatted using a filesystem that supports holes in file (e.g. ext4)

## Typical usage

Create a file with deterministic random sequences:

```
% flashtest -w /mnt/sdcard/testfile
```

(FlashTest refuses to overwrite an existing file.)
You can unmount the flash storage after writing the file, or you can choose to flush the cache instead:

```
# echo 1 > /proc/sys/vm/drop_caches
```

Optionally, detach the flash storage from the system and keep it in drawer for weeks, monthes or years if you like.

To verify the content of the random number sequence:

```
% flashtest -r /mnt/sdcard/testfile
```

FlashTest will report errors if it finds any flipped bits in the file.
Hope you can find an interesting error!

## License

Public domain.

## Warning

I found a faulty CompactFlash using this program.
It looked okay after running this program for two times, but after detaching the card from the host, the card started to answer 0xffffffff to READ CAPACITY command.

In short: this program can break your flash storage. Use it at your own risk.

(p.s. the CF had a lifetime warranty, so it cost me nothing to get a new one.)
