class FlashVerifier: public FlashTest {
public:
	FlashVerifier(const char* path):FlashTest(path) {};
	virtual ~FlashVerifier(){}
protected:
	int openMode() { return O_RDONLY; };
	uintmax_t mapSize();
	int mapMode() { return PROT_READ; };
	int testWordAt(uintmax_t i);
};
uintmax_t
FlashVerifier::mapSize() {
	return fs::file_size(m_path);
}
int
FlashVerifier::testWordAt(uintmax_t i) {
	auto correct=m_rand();
	if(m_region[i] != correct) {
		std::cout << "=== VERIFICATION ERROR AT OFFSET " << std::dec << (i*sizeof(uint64_t)) << " ===" << std::endl
			  << "Expected: 0x" << std::hex << correct << std::endl
			  << "Found:    0x" << m_region[i] << std::endl;
		return 1;
	}
	return 0;
}
