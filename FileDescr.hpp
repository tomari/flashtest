// Exists just to make sure to close fd when exception occurs
class FileDescr {
public:
	FileDescr(const char* path, int flags, mode_t mode):
		fd(open(path,flags,mode)) {
		if(fd<0) {
			throw std::runtime_error(strerror(errno));
		}
	}
	~FileDescr() {
		close(fd);
	}
	int fd;
};
