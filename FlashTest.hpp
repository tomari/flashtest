#include <algorithm>
#include <chrono>
class Runnable{
public:
	virtual int run()=0;
};
class FlashTest:public Runnable{
public:
	FlashTest(const char* path);
	~FlashTest();
	virtual int run()override;
protected:
	virtual int openMode() = 0;
	virtual uintmax_t mapSize() = 0;
	virtual int mapMode() = 0;
	virtual int testWordAt(uintmax_t i) = 0;
	int test();
	unsigned long pagesizeMask();
	const char* m_path;
	uint64_t *m_region;
	uintmax_t m_bytes;
	std::mt19937_64 m_rand;
};

FlashTest::FlashTest(const char* path):
	m_path(path),
	m_region(NULL),
	m_bytes(0ull)
{
}
FlashTest::~FlashTest() {
	if(m_region) {
		munmap(m_region,m_bytes);
	}
}
unsigned long
FlashTest::pagesizeMask() {
	auto pagesize=sysconf(_SC_PAGESIZE);
	return pagesize-1ul;
}
int
FlashTest::run() {
	do {
		auto fd=FileDescr(m_path,this->openMode(),S_IRUSR|S_IWUSR);
		m_bytes=this->mapSize();
		auto mapmode=this->mapMode();
		std::cout << "mapping " << m_bytes << " bytes at " << m_path << std::endl;
		m_region=static_cast<uint64_t*>(mmap(NULL,m_bytes,mapmode,MAP_SHARED,fd.fd,0ul));
		auto mmaperr=errno;
		if(m_region==MAP_FAILED) {
			throw std::runtime_error(strerror(mmaperr));
		}
	} while(false); // fd goes out of scope
	return this->test();
}
namespace clk = std::chrono;
int
FlashTest::test() {
	auto nwords=m_bytes/sizeof(uint64_t);
	const auto p=128ul;
	auto nwordsp=(nwords+p)/p;
	auto i=0ull;
	auto j=0ull;
	auto start_time=clk::high_resolution_clock::now();
	auto error_count=0ull;
	for(j=1; j<=p; j++) {
		auto goal=std::min(static_cast<uintmax_t>(j*nwordsp),static_cast<uintmax_t>(nwords));
		for( ; i<goal; i++) {
			error_count+=this->testWordAt(i)?1ull:0ull;
		}
		auto done_bytes=i*sizeof(uint64_t);
		auto now=clk::high_resolution_clock::now();
		auto elapsed_ms=clk::duration_cast<clk::milliseconds>(now-start_time).count();
		auto speed=(1000ull*done_bytes)/std::max(elapsed_ms,1l); // avoid SIGFPE
		std::cerr << std::dec << done_bytes << " bytes ( "
			<< ((100ull*i)/nwords) << " %) in "
			<< elapsed_ms << " ms ( "
			<< speed << " bytes/s)"
			<< '\r' << std::flush;
	}
	std::cerr << std::endl;
	return error_count?1:0;
}
