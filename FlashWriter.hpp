class FlashWriter: public FlashTest {
public:
	FlashWriter(const char* path):FlashTest(path){};
	virtual ~FlashWriter(){};
protected:
	uintmax_t mapSize()override;
	int openMode() { return O_EXCL|O_CREAT|O_RDWR; };
	int mapMode() { return PROT_WRITE|PROT_READ; };
	int testWordAt(uintmax_t i) { m_region[i] = m_rand(); return 0;}
	void test();
private:
	virtual unsigned long requested_bytes()=0;
};
uintmax_t
FlashWriter::mapSize(){
	auto fourK=pagesizeMask();
	auto mapsize=(requested_bytes()+fourK)&~fourK;
	if(truncate(m_path,static_cast<off_t>(mapsize))) {
		throw std::runtime_error(strerror(errno));
	}
	return mapsize;
}
// class to handle requests to fill in percentage
class FlashWriterPercentage:public FlashWriter{
public:
	FlashWriterPercentage(const char* path,double percentage)
		:FlashWriter(path)
		,m_fill_percent(percentage)
	{}
	virtual ~FlashWriterPercentage(){}
private:	
	virtual unsigned long requested_bytes()override;
	const double m_fill_percent;
};
uintmax_t
FlashWriterPercentage::requested_bytes() {
	auto spacei=fs::space(m_path);
	auto free_bytes=geteuid()?spacei.available:spacei.free;
	return static_cast<unsigned long>(static_cast<double>(free_bytes)*m_fill_percent);
}

// Class to handle requests to fill in megabytes
class FlashWriterMB:public FlashWriter{
public:
	FlashWriterMB(const char* path,uintmax_t fill_size_mb)
		:FlashWriter(path)
		,m_fill_size_mb(fill_size_mb)
	{}
	virtual ~FlashWriterMB(){}
private:
	virtual unsigned long requested_bytes()override{
		return 1024ul*1024ul*m_fill_size_mb;
	}
	const uintmax_t m_fill_size_mb;
};
